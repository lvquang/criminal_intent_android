package com.quangluu.demo.criminalintent

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class CrimeAdapter(val mContext: Context,
                   val items: List<Crime>,
                   val listener: (Crime) -> Unit) : RecyclerView.Adapter<CrimeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_crime, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], listener)

    override fun getItemCount(): Int = items.size

    inner class CrimeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.crime_title)
        val date: TextView = itemView.findViewById(R.id.crime_date)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.crime_title)
        val date: TextView = itemView.findViewById(R.id.crime_date)

        fun bind(item: Crime, listener: (Crime) -> Unit) = with(itemView) {
            title.text = item.mTitle
            date.text = item.mDate.toString()

            setOnClickListener {
                listener(item) }
        }
    }

}
