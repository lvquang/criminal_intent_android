package com.quangluu.demo.criminalintent

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.util.ArrayList

class CrimeListFragment: Fragment() {
    private lateinit var mCrimeRecyclerView: RecyclerView
    private lateinit var mAdapter: CrimeAdapter
    var crimes = ArrayList<Crime>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.fragment_crime_list,
                container, false)
        mCrimeRecyclerView = view.findViewById(R.id.crime_recycle_view)
        mCrimeRecyclerView.layoutManager = LinearLayoutManager(activity)

        updateUI()

        return  view
    }

    fun updateUI() {
        crimes = getList()
        mAdapter = CrimeAdapter(context!!,crimes, listener = {
            println("${it.mTitle} Clicked")
        })
        mCrimeRecyclerView.adapter = mAdapter
    }

    fun getList() = CrimeLab.getInstance().crimes
}