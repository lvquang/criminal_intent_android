package com.quangluu.demo.criminalintent

import java.util.*

data class Crime(val mId: UUID = UUID.randomUUID(),
                 var mTitle: String = "",
                 val mDate: Date = Date(),
                 var mSolved: Boolean = false,
                 var mRequiredPolice: Boolean = false)