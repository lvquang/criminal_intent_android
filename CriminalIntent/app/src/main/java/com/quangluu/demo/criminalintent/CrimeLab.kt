package com.quangluu.demo.criminalintent

import android.util.Log
import java.util.*

public class CrimeLab private constructor() {
    var crimes = ArrayList<Crime>()

    init {
        // define in constructor
        for (i in 0..99) {
            val crime = Crime()
            crime.mTitle = "Crime #$i"
            crime.mSolved = i % 2 == 0
            crimes.add(crime)
        }
    }

    private object Holder { val INSTANCE = CrimeLab() }

    companion object {
        @JvmStatic
        fun getInstance(): CrimeLab{
            return Holder.INSTANCE
        }
    }

    fun getCrime(uuid: UUID):Crime? = crimes.find { it.mId.equals(uuid) }
}