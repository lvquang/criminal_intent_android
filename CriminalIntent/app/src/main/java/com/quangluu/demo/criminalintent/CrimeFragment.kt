package com.quangluu.demo.criminalintent

import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

class CrimeFragment: Fragment() {
    private lateinit var mCrime: Crime
    private lateinit var mTextField:EditText
    private lateinit var mDateButton:Button
    private lateinit var mSolvedCheckbox:CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mCrime = Crime()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_crime,
                container, false)
        mTextField = view.findViewById(R.id.crime_title)
        mTextField.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                mCrime.mTitle = s.toString()
            }

            override fun afterTextChanged(s: Editable?) {

            }
        })
        mDateButton = view.findViewById(R.id.crime_date)
        mDateButton.text = mCrime.mDate.toString()
        mDateButton.isEnabled = false
        mSolvedCheckbox = view.findViewById(R.id.crime_solved)
        mSolvedCheckbox.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                mCrime.mSolved = isChecked
            }
        })

        return view
    }
}